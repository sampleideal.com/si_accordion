import 'package:flutter/material.dart';

import 'accordion_cell.dart';
import 'accordion_cell_style.dart';
import 'accordion_divider_widget.dart';
import 'accordion_title_style.dart';

class SIAccordionChildWidget extends StatefulWidget {
  const SIAccordionChildWidget({
    super.key,
    required this.accordionCell,
    required this.isOpen,
    required this.onTapOpen,
    this.unifiedTitleStyle,
    this.unifiedCellStyle,
  });

  /// accordion cell class
  final SIAccordionCell accordionCell;

  /// is open
  final bool isOpen;

  /// on tap open
  final void Function() onTapOpen;

  /// unified title style
  final SIAccordionTitleStyle? unifiedTitleStyle;

  /// unified cell style
  final SIAccordionCellStyle? unifiedCellStyle;
  @override
  State<SIAccordionChildWidget> createState() => _SIAccordionChildWidgetState();
}

class _SIAccordionChildWidgetState extends State<SIAccordionChildWidget> with SingleTickerProviderStateMixin {
  /// animation controller
  late AnimationController _controller;

  /// size animation
  late Animation<double> _animationSize;

  /// rotation animation
  late Animation<double> _animationRotation;

  /// animation time
  final Duration _duration = const Duration(milliseconds: 200);

  /// title style
  late SIAccordionTitleStyle _titleStyle;

  /// cell style
  late SIAccordionCellStyle _cellStyle;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _duration, vsync: this);
    _animationSize = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _controller, curve: Curves.linear),
    );
    _animationRotation = Tween(begin: 0.0, end: 0.5).animate(
      CurvedAnimation(parent: _controller, curve: Curves.linear),
    );
    setValue();
  }

  @override
  void didUpdateWidget(covariant SIAccordionChildWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isOpen) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  /// 設定參數
  void setValue() {
    _titleStyle = widget.accordionCell.titleStyle ?? widget.unifiedTitleStyle ?? SIAccordionTitleStyle();
    _cellStyle = widget.accordionCell.cellStyle ?? widget.unifiedCellStyle ?? SIAccordionCellStyle();
  }

  /// 回傳點擊
  void onTapOpen() {
    widget.onTapOpen();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            Material(
              color: _titleStyle.backgroundColor,
              child: InkWell(
                highlightColor: _titleStyle.highlightColor,
                onTap: onTapOpen,
                child: Padding(
                  padding: _titleStyle.titlePadding,
                  child: Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: _titleStyle.titleTextPadding,
                          child: Text(
                            widget.accordionCell.title,
                            maxLines: 1,
                            style: _titleStyle.titleStyle,
                          ),
                        ),
                      ),
                      RotationTransition(
                        turns: _animationRotation,
                        child: _titleStyle.onOpenIcon,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SIAccordionDividerWidget(
              indent: _titleStyle.borderIndent,
              color: _titleStyle.borderColor,
              height: _titleStyle.borderHeight,
            ),
          ],
        ),
        SizeTransition(
          sizeFactor: _animationSize,
          child: Column(
            children: [
              if (widget.accordionCell.child != null) widget.accordionCell.child!,
              SIAccordionDividerWidget(
                indent: _cellStyle.borderIndent,
                color: _cellStyle.borderColor,
                height: _cellStyle.borderHeight,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
