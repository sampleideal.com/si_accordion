import 'package:flutter/material.dart';

class SIAccordionTitleStyle {
  /// set accordion title style
  SIAccordionTitleStyle({
    this.titlePadding = const EdgeInsets.all(8.0),
    this.backgroundColor,
    this.highlightColor,
    this.titleTextPadding = EdgeInsets.zero,
    this.titleStyle = const TextStyle(
      fontSize: 16.0,
      fontWeight: FontWeight.bold,
      overflow: TextOverflow.ellipsis,
    ),
    this.onTapIcon = const Icon(Icons.keyboard_arrow_right, size: 20),
    this.selectValuePadding = const EdgeInsets.symmetric(horizontal: 8.0),
    this.selectValueStyle = const TextStyle(
      fontSize: 16.0,
      color: Colors.black54,
    ),
    this.onOpenIcon = const Icon(Icons.keyboard_arrow_up, size: 20),
    this.borderColor = Colors.black26,
    this.borderIndent = 8.0,
    this.borderHeight = 0.0,
  });

  /// title padding
  final EdgeInsetsGeometry titlePadding;

  /// background color
  final Color? backgroundColor;

  /// on tap highlight color
  final Color? highlightColor;

  /// text padding
  final EdgeInsetsGeometry titleTextPadding;

  /// title style
  final TextStyle titleStyle;

  /// only on tap icon
  final Icon onTapIcon;

  /// selected value padding
  final EdgeInsetsGeometry selectValuePadding;

  /// selected value style
  final TextStyle selectValueStyle;

  /// has child on tap icon
  final Icon onOpenIcon;

  /// border color
  final Color borderColor;

  /// border indent
  final double borderIndent;

  /// border height
  final double borderHeight;
}
