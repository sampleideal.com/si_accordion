import 'package:flutter/material.dart';

import 'accordion_cell.dart';
import 'accordion_divider_widget.dart';
import 'accordion_title_style.dart';

class SIAccordionTitleWidget extends StatelessWidget {
  const SIAccordionTitleWidget({
    super.key,
    required this.accordionCell,
    this.unifiedTitleStyle,
  });

  /// accordion cell class
  final SIAccordionCell accordionCell;

  /// unified title style
  final SIAccordionTitleStyle? unifiedTitleStyle;

  @override
  Widget build(BuildContext context) {
    var style = accordionCell.titleStyle ?? unifiedTitleStyle ?? SIAccordionTitleStyle();
    return Column(
      children: [
        Container(
          color: style.backgroundColor,
          padding: style.titlePadding,
          alignment: Alignment.center,
          child: Text(
            accordionCell.title,
            maxLines: 1,
            style: style.titleStyle,
          ),
        ),
        SIAccordionDividerWidget(
          indent: style.borderIndent,
          color: style.borderColor,
          height: style.borderHeight,
        ),
      ],
    );
  }
}
