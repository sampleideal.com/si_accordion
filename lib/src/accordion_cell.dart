import 'package:flutter/material.dart';

import 'accordion_cell_style.dart';
import 'accordion_title_style.dart';

class SIAccordionCell<T> {
  SIAccordionCell({
    required this.title,
    this.onTap,
    this.onTapCallBackKey,
    this.childMap,
    this.initSelectKey,
    this.child,
    this.titleStyle,
    this.cellStyle,
  }) : assert(
          onTap == null || onTapCallBackKey == null || child == null,
          '''onTap or onTapCallBackKey or child can only set one of them,\n
          Or do not set it at all,\n
          Setting onTapCallBackKey requires setting childMap,\n
          If initSelectKey is not set, the first childMap will be used as the initial value.
          ''',
        );

  /// title
  final String title;

  /// on tap
  final VoidCallback? onTap;

  /// on tap call back key
  final void Function(T)? onTapCallBackKey;

  /// child map
  final Map<T, String>? childMap;

  /// init select child map key
  final T? initSelectKey;

  /// child widget
  final Widget? child;

  /// set only this widget title style
  final SIAccordionTitleStyle? titleStyle;

  ///  set only this widget cell style
  final SIAccordionCellStyle? cellStyle;

  /// on tap call back key function
  void onTapCallBack(T backKey) {
    if (onTapCallBackKey != null && childMap != null) {
      onTapCallBackKey!(backKey);
    }
  }

  /// check has set call back parameter
  bool hasCallBack() {
    return onTapCallBackKey != null && childMap != null && childMap!.isNotEmpty;
  }
}
