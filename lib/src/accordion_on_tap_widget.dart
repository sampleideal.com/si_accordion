import 'package:flutter/material.dart';

import 'accordion_cell.dart';
import 'accordion_divider_widget.dart';
import 'accordion_title_style.dart';

class SIAccordionOnTapWidget extends StatelessWidget {
  const SIAccordionOnTapWidget({
    super.key,
    required this.accordionCell,
    this.unifiedTitleStyle,
  });

  /// accordion cell class
  final SIAccordionCell accordionCell;

  /// unified title style
  final SIAccordionTitleStyle? unifiedTitleStyle;

  @override
  Widget build(BuildContext context) {
    var style = accordionCell.titleStyle ?? unifiedTitleStyle ?? SIAccordionTitleStyle();
    return Column(
      children: [
        Material(
          color: style.backgroundColor,
          child: InkWell(
            highlightColor: style.highlightColor,
            onTap: accordionCell.onTap,
            child: Padding(
              padding: style.titlePadding,
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: style.titleTextPadding,
                      child: Text(
                        accordionCell.title,
                        maxLines: 1,
                        style: style.titleStyle,
                      ),
                    ),
                  ),
                  style.onTapIcon,
                ],
              ),
            ),
          ),
        ),
        SIAccordionDividerWidget(
          indent: style.borderIndent,
          color: style.borderColor,
          height: style.borderHeight,
        ),
      ],
    );
  }
}
