import 'package:flutter/material.dart';

import 'accordion_call_back_widget.dart';
import 'accordion_cell.dart';
import 'accordion_cell_style.dart';
import 'accordion_child_widget.dart';
import 'accordion_on_tap_widget.dart';
import 'accordion_title_style.dart';
import 'accordion_title_widget.dart';

class SIAccordion extends StatefulWidget {
  const SIAccordion({
    super.key,
    this.children = const [],
    this.physics = const ClampingScrollPhysics(),
    this.titleStyle,
    this.cellStyle,
    this.openCount = 1,
    this.spacing = 0.0,
  });

  /// accordion cell class list
  final List<SIAccordionCell> children;

  /// physics
  final ScrollPhysics physics;

  /// unified widget title style
  final SIAccordionTitleStyle? titleStyle;

  /// unified widget cell style
  final SIAccordionCellStyle? cellStyle;

  /// can open count
  final int openCount;

  /// spacing
  final double spacing;
  @override
  State<SIAccordion> createState() => _SIAccordionState();
}

class _SIAccordionState extends State<SIAccordion> {
  late List<bool> _recordOpenClose;
  final List<int> _recordOpenList = [];

  @override
  void initState() {
    super.initState();
    setValue();
  }

  @override
  void didUpdateWidget(covariant SIAccordion oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.children.length != widget.children.length) {
      setValue();
    }
  }

  /// set value
  void setValue() => _recordOpenClose = List.filled(widget.children.length, false);

  /// open index
  void onTapOpen(int index) {
    if (!_recordOpenList.contains(index)) {
      _recordOpenList.add(index);
      if (_recordOpenList.length > widget.openCount) {
        int removerIndex = _recordOpenList.removeAt(0);
        _recordOpenClose[removerIndex] = !_recordOpenClose[removerIndex];
      }
    } else {
      _recordOpenList.remove(index);
    }
    _recordOpenClose[index] = !_recordOpenClose[index];
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: widget.physics,
      itemCount: widget.children.length,
      itemBuilder: (context, index) {
        return Column(
          children: [
            listWidget(index),
            SizedBox(height: widget.spacing),
          ],
        );
      },
    );
  }

  Widget listWidget(int index) {
    final SIAccordionCell child = widget.children[index];
    if (child.onTap != null) {
      return SIAccordionOnTapWidget(
        accordionCell: child,
        unifiedTitleStyle: widget.titleStyle,
      );
    } else if (child.hasCallBack()) {
      return SIAccordionCallBackWidget(
        accordionCell: child,
        isOpen: _recordOpenClose[index],
        onTapOpen: () => onTapOpen(index),
        unifiedTitleStyle: widget.titleStyle,
        unifiedCellStyle: widget.cellStyle,
      );
    } else if (child.child != null) {
      return SIAccordionChildWidget(
        accordionCell: child,
        isOpen: _recordOpenClose[index],
        onTapOpen: () => onTapOpen(index),
        unifiedTitleStyle: widget.titleStyle,
        unifiedCellStyle: widget.cellStyle,
      );
    }
    return SIAccordionTitleWidget(
      accordionCell: child,
      unifiedTitleStyle: widget.titleStyle,
    );
  }
}
