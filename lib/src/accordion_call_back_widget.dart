import 'package:flutter/material.dart';

import 'accordion_cell.dart';
import 'accordion_cell_style.dart';
import 'accordion_divider_widget.dart';
import 'accordion_title_style.dart';

class SIAccordionCallBackWidget extends StatefulWidget {
  const SIAccordionCallBackWidget({
    super.key,
    required this.accordionCell,
    required this.isOpen,
    required this.onTapOpen,
    this.unifiedTitleStyle,
    this.unifiedCellStyle,
  });

  /// accordion cell class
  final SIAccordionCell accordionCell;

  /// is open
  final bool isOpen;

  /// on tap open
  final void Function() onTapOpen;

  /// unified title style
  final SIAccordionTitleStyle? unifiedTitleStyle;

  /// unified cell style
  final SIAccordionCellStyle? unifiedCellStyle;
  @override
  State<SIAccordionCallBackWidget> createState() => _SIAccordionCallBackWidgetState();
}

class _SIAccordionCallBackWidgetState extends State<SIAccordionCallBackWidget> with SingleTickerProviderStateMixin {
  /// animation controller
  late AnimationController _controller;

  /// size animation
  late Animation<double> _animationSize;

  /// rotation animation
  late Animation<double> _animationRotation;

  /// animation time
  final Duration _duration = const Duration(milliseconds: 200);

  /// record map value to list
  late List<String> _recordValue;

  /// select value
  late String _selectValue;

  /// title style
  late SIAccordionTitleStyle _titleStyle;

  /// cell style
  late SIAccordionCellStyle _cellStyle;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _duration, vsync: this);
    _animationSize = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _controller, curve: Curves.linear),
    );
    _animationRotation = Tween(begin: 0.0, end: 0.5).animate(
      CurvedAnimation(parent: _controller, curve: Curves.linear),
    );
    setValue();
  }

  @override
  void didUpdateWidget(covariant SIAccordionCallBackWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.accordionCell.childMap != widget.accordionCell.childMap) {
      setValue();
    }
    if (widget.isOpen) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  /// set value
  void setValue() {
    _recordValue = widget.accordionCell.childMap!.values.toList();
    if (widget.accordionCell.initSelectKey != null && widget.accordionCell.childMap!.containsKey(widget.accordionCell.initSelectKey)) {
      _selectValue = widget.accordionCell.childMap![widget.accordionCell.initSelectKey]!;
    } else {
      _selectValue = _recordValue[0];
    }
    _titleStyle = widget.accordionCell.titleStyle ?? widget.unifiedTitleStyle ?? SIAccordionTitleStyle();
    _cellStyle = widget.accordionCell.cellStyle ?? widget.unifiedCellStyle ?? SIAccordionCellStyle();
  }

  /// on tap open
  void onTapOpen() {
    widget.onTapOpen();
  }

  /// on tap call back key
  void onTapValue(int index) {
    widget.accordionCell.onTapCallBack(
      widget.accordionCell.childMap!.keys.firstWhere(
        (key) => widget.accordionCell.childMap![key] == _recordValue[index],
      ),
    );
    setState(() => _selectValue = _recordValue[index]);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            Material(
              color: _titleStyle.backgroundColor,
              child: InkWell(
                highlightColor: _titleStyle.highlightColor,
                onTap: onTapOpen,
                child: Padding(
                  padding: _titleStyle.titlePadding,
                  child: Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: _titleStyle.titleTextPadding,
                          child: Text(
                            widget.accordionCell.title,
                            maxLines: 1,
                            style: _titleStyle.titleStyle,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 0,
                        child: Padding(
                          padding: _titleStyle.selectValuePadding,
                          child: Text(
                            _selectValue,
                            maxLines: 1,
                            style: _titleStyle.selectValueStyle,
                          ),
                        ),
                      ),
                      RotationTransition(
                        turns: _animationRotation,
                        child: _titleStyle.onOpenIcon,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SIAccordionDividerWidget(
              indent: _titleStyle.borderIndent,
              color: _titleStyle.borderColor,
              height: _titleStyle.borderHeight,
            ),
          ],
        ),
        SizeTransition(
          sizeFactor: _animationSize,
          child: Column(
            children: List.generate(
              widget.accordionCell.childMap!.length,
              (index) {
                return onTapWidget(
                  value: _recordValue[index],
                  onTapValue: () => onTapValue(index),
                  isSelect: _recordValue[index] == _selectValue,
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget onTapWidget({
    required String value,
    required void Function() onTapValue,
    required bool isSelect,
  }) {
    Color? bgColor = isSelect && _cellStyle.selectedBackgroundColor != null ? _cellStyle.selectedBackgroundColor : _cellStyle.backgroundColor;
    return Column(
      children: [
        Material(
          color: bgColor,
          child: InkWell(
            highlightColor: _cellStyle.highlightColor,
            onTap: onTapValue,
            child: Padding(
              padding: _cellStyle.cellPadding,
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: _cellStyle.labelPadding,
                      child: Text(
                        value,
                        maxLines: 1,
                        style: _cellStyle.labelStyle,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: isSelect,
                    maintainAnimation: true,
                    maintainState: true,
                    maintainSize: true,
                    child: _cellStyle.selectedIcon,
                  ),
                ],
              ),
            ),
          ),
        ),
        SIAccordionDividerWidget(
          indent: _cellStyle.borderIndent,
          color: _cellStyle.borderColor,
          height: _cellStyle.borderHeight,
        ),
      ],
    );
  }
}
