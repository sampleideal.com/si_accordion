import 'package:flutter/material.dart';

class SIAccordionCellStyle {
  /// cell style
  SIAccordionCellStyle({
    this.cellPadding = const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
    this.backgroundColor,
    this.highlightColor,
    this.labelPadding = EdgeInsets.zero,
    this.labelStyle = const TextStyle(
      overflow: TextOverflow.ellipsis,
      fontSize: 14,
    ),
    this.selectedIcon = const Icon(Icons.check, size: 20),
    this.selectedBackgroundColor,
    this.borderColor = Colors.black26,
    this.borderIndent = 16.0,
    this.borderHeight = 0.0,
  });

  /// cell padding
  final EdgeInsetsGeometry cellPadding;

  /// background color
  final Color? backgroundColor;

  /// on tap highlight color
  final Color? highlightColor;

  /// text padding
  final EdgeInsetsGeometry labelPadding;

  /// text style
  final TextStyle labelStyle;

  /// selected icon
  final Icon selectedIcon;

  /// selected background color
  final Color? selectedBackgroundColor;

  /// border color
  final Color borderColor;

  /// border indent
  final double borderIndent;

  /// border height
  final double borderHeight;
}
