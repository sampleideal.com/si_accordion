import 'package:flutter/material.dart';

class SIAccordionDividerWidget extends StatelessWidget {
  const SIAccordionDividerWidget({
    super.key,
    required this.indent,
    required this.color,
    required this.height,
  });

  /// indent
  final double indent;

  /// color
  final Color color;

  /// height
  final double height;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: indent),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: color,
              width: height,
            ),
          ),
        ),
      ),
    );
  }
}
