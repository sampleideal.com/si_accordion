# SIAccordion
This is a flutter accordion package written in dart for flutter developers.
## Display
|  |  |
| ------------ | ------------ |
| ![](https://gitlab.com/sampleideal.com/si_accordion/-/raw/main/example/assets/accordion_gif_01.gif) |  ![](https://gitlab.com/sampleideal.com/si_accordion/-/raw/main/example/assets/accordion_gif_02.gif) |
## Getting Started
### SIAccordion
| Parameters | Categories | Description |
| ------------ | ------------ | ------------ |
| children | List < SIAccordionCell > | widget |
| titleStyle | SIAccordionTitleStyle? | Uniformly set the display parameters of the title |
| cellStyle | SIAccordionCellStyle? | Uniformly set the display parameters of the cell |
| physics | ScrollPhysics | The default is ClampingScrollPhysics |
| openCount | int | The number that can be expanded at the same time |
| spacing | double | Each cell spacing |
### SIAccordionCell < T >
| Parameters | Categories | Description |
| ------------ | ------------ | ------------ |
| title | String | title of content option |
| onTap | VoidCallback? | Execute function after tap |
| onTapCallBackKey | void Function(T)? | Return the key after clicking the content option |
| childMap | Map < T, String >? | Content options when accordion is turned on |
| initSelectKey | T? | key to set initial selection |
| child | Widget? | key to set initial selection |
| titleStyle | SIAccordionTitleStyle? | Set the displayed parameters of the title, and those set by SIAccordion will be ignored |
| cellStyle | SIAccordionCellStyle? | Set the displayed parameters of the cell, and those set by SIAccordion will be ignored |

### SIAccordionTitleStyle
| Parameters | Categories | Description |
| ------------ | ------------ | ------------ |
| titlePadding | EdgeInsetsGeometry | Padding of the entire title block |
| backgroundColor | Color? | The background color of the title block |
| highlightColor | Color? | clicked wave color |
| titleTextPadding | EdgeInsetsGeometry | Title text padding |
| titleStyle | TextStyle | title text style |
| onTapIcon | Icon | The icon displayed on the right when there is only onTap parameter |
| selectValuePadding | EdgeInsetsGeometry | title shows the filling of the cell selection parameters |
| selectValueStyle | TextStyle | title displays the style of cell selection parameters |
| onOpenIcon | Icon | If there are child components, click to expand the icon displayed on the right. |
| borderColor | Color | Bottom hem color |
| borderIndent | double | Bottom hem indent |
| borderHeight | double | Bottom hem height |
### SIAccordionCellStyle
| Parameters | Categories | Description |
| ------------ | ------------ | ------------ |
| cellPadding | EdgeInsetsGeometry | Padding of the entire cell block |
| backgroundColor | Color? | The background color of the cell block |
| highlightColor | Color? | clicked wave color |
| labelPadding | EdgeInsetsGeometry | cell text padding |
| labelStyle | TextStyle | cell text style |
| selectedIcon | Icon | cell The icon displayed to the right of the selected parameter |
| selectedBackgroundColor | Color? | cell The background color of the selected parameter |
| borderColor | Color | Bottom hem color |
| borderIndent | double | Bottom hem indent |
| borderHeight | double | Bottom hem height |