## 0.0.1

* Initial version

## 0.0.2

* Update the scores documentation

## 0.0.3

* Update the scores documentation

## 0.1.0

* Modify widget presentation mode, animate expansion, and customize widget expansion.