import 'package:flutter/material.dart';
import 'package:si_accordion/si_accordion.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Accordion Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const AccordionDemoView(
        title: 'Accordion Demo Page',
      ),
    );
  }
}

class AccordionDemoView extends StatelessWidget {
  const AccordionDemoView({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: SafeArea(
          child: SIAccordion(
            children: [
              SIAccordionCell(title: "This Demo"),
              SIAccordionCell(
                title: "User info",
                onTap: () => debugPrint("Open user info view."),
              ),
              SIAccordionCell<LanguageEnum>(
                title: "Language",
                childMap: {
                  LanguageEnum.en: "English",
                  LanguageEnum.tw: "繁體中文",
                  LanguageEnum.cn: "简体中文",
                },
                initSelectKey: LanguageEnum.en,
                onTapCallBackKey: (mapKey) => debugPrint("$mapKey"),
              ),
              SIAccordionCell<String>(
                title: "Appearance",
                childMap: {
                  "night": "Night",
                  "light": "Light",
                },
                initSelectKey: "light",
                onTapCallBackKey: (mapKey) => debugPrint(mapKey),
              ),
              SIAccordionCell<bool>(
                title: "Remind",
                childMap: {
                  false: "OFF",
                  true: "ON",
                },
                initSelectKey: true,
                onTapCallBackKey: (mapKey) => debugPrint("$mapKey"),
              ),
              SIAccordionCell(
                title: "About us",
                onTap: () => debugPrint("Open about us view"),
              ),
            ],
          ),
        ),
      ),
      appBar: AppBar(
        title: Text(title),
      ),
      body: SIAccordion(
        titleStyle: SIAccordionTitleStyle(
          backgroundColor: Colors.black12,
        ),
        cellStyle: SIAccordionCellStyle(
          backgroundColor: Colors.black12,
          selectedBackgroundColor: Colors.black26,
          borderColor: Colors.transparent,
        ),
        spacing: 5,
        children: [
          SIAccordionCell(title: "This Demo"),
          SIAccordionCell(
            title: "User info",
            onTap: () => debugPrint("Open user info view."),
          ),
          SIAccordionCell<LanguageEnum>(
            title: "Language",
            childMap: {
              LanguageEnum.en: "English",
              LanguageEnum.tw: "繁體中文",
              LanguageEnum.cn: "简体中文",
            },
            initSelectKey: LanguageEnum.en,
            onTapCallBackKey: (mapKey) => debugPrint("$mapKey"),
            titleStyle: SIAccordionTitleStyle(
              backgroundColor: Colors.yellow.withOpacity(0.5),
            ),
            cellStyle: SIAccordionCellStyle(
              backgroundColor: Colors.yellow.withOpacity(0.2),
              selectedBackgroundColor: Colors.red.withOpacity(0.3),
            ),
          ),
          SIAccordionCell<String>(
            title: "Appearance",
            childMap: {
              "night": "Night",
              "light": "Light",
            },
            initSelectKey: "light",
            onTapCallBackKey: (mapKey) => debugPrint(mapKey),
          ),
          SIAccordionCell<bool>(
            title: "Remind",
            childMap: {
              false: "OFF",
              true: "ON",
            },
            initSelectKey: true,
            onTapCallBackKey: (mapKey) => debugPrint("$mapKey"),
            titleStyle: SIAccordionTitleStyle(
              backgroundColor: Colors.deepPurple.withOpacity(0.2),
            ),
          ),
          SIAccordionCell(
            title: "About us",
            onTap: () => debugPrint("Open about us view"),
          ),
          SIAccordionCell(
            title: "Explanatory text",
            child: Container(
              color: Colors.cyanAccent.withOpacity(0.2),
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: const Text('This is a large string of explanatory text, just to show the accordion widget, expand to display the description.'),
            ),
            titleStyle: SIAccordionTitleStyle(
              backgroundColor: Colors.cyanAccent.withOpacity(0.2),
            ),
          ),
        ],
      ),
    );
  }
}

enum LanguageEnum { en, cn, tw }
